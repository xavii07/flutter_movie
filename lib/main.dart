import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
} //SOLA PARA LA CLASE

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              'Opening this week',
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
            SizedBox(
              width: 125,
              height: 25,
              child: TextButton(
                style: TextButton.styleFrom(
                  backgroundColor: Colors.yellow,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 3,
                    vertical: 3,
                  ),
                ),
                onPressed: () {},
                child: const Text(
                  'Now Playing',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            const Text(
              'Coming Soon',
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ],
        ),
        backgroundColor: Colors.black,
      ),
      body: Container(
        color: Colors.grey[900],
        child: Center(
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                const Expanded(
                  child: SizedBox(
                    height: 300,
                    child: Image(
                      image: NetworkImage(
                        'https://s1.eestatic.com/2019/04/01/cultura/series/juego_de_tronos-series-series_387722218_119349187_1706x960.jpg',
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const Expanded(
                  child: SizedBox(
                    height: 300,
                    child: Image(
                      image: NetworkImage(
                        'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcR6T5cjioh6q22GgbcfNcq_i7XXR_UUA6K9n1DzKjze2KYfv6-o',
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Column(
                  children: [
                    const SizedBox(
                      width: 50,
                      height: 100,
                      child: Icon(
                        Icons.play_circle,
                        color: Colors.yellow,
                        size: 50,
                      ),
                    ),
                    const SizedBox(
                      width: 50,
                      height: 100,
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.yellow,
                        size: 60,
                      ),
                    ),
                    Container(
                      width: 80,
                      height: 100,
                      color: Colors.yellow,
                      child: const Icon(
                        Icons.hdr_plus_rounded,
                        color: Colors.black,
                        size: 50,
                      ),
                    ),
                  ],
                )
              ],
            ),
            const SizedBox(height: 20),
            const Text(
              'BROWSE MORE TRAILERS',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 20),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 200,
                        child: Image(
                          image: NetworkImage(
                            'https://play-lh.googleusercontent.com/vdC-3zQ3K1133Qvcl-71O9ulLeu1IsehUWc_2phxEFfy3lo27R3LOa2rbr3xK5YUMY1s=w240-h480-rw',
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(height: 8),
                      Text(
                        'DEADPOOL',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontSize: 10,
                            backgroundColor: Colors.black),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 8),
                Flexible(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 200,
                        child: Image(
                          image: NetworkImage(
                            'https://pics.filmaffinity.com/batman_v_superman_dawn_of_justice-728293826-mmed.jpg',
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(height: 8),
                      Text(
                        'BATMAN Y SUPERMAN',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontSize: 10,
                            backgroundColor: Colors.black),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 8),
                Flexible(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 200,
                        child: Image(
                          image: NetworkImage(
                            'https://image.api.playstation.com/vulcan/ap/rnd/202010/0114/b4Q1XWYaTdJLUvRuALuqr0wP.png',
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(height: 8), // Espacio entre la imagen y el texto
                      Text(
                        'DOOM',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontSize: 10,
                            backgroundColor: Colors.black),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 40),
            Container(
              color: Colors.black,
              padding: const EdgeInsets.all(10),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image(
                    width: 125,
                    image: NetworkImage(
                      'https://t3.ftcdn.net/jpg/02/43/12/34/360_F_243123463_zTooub557xEWABDLk0jJklDyLSGl2jrr.jpg',
                    ),
                    fit: BoxFit.contain,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            'LATEST NEWS | Movies',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              backgroundColor: Colors.black,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            'John Wick 4: Fecha de estreno, reparto, trama',
                            style: TextStyle(
                              color: Colors.yellow,
                              fontSize: 12,
                              backgroundColor: Colors.black,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam auctor, nunc nec ultricies. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam auctor,',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              backgroundColor: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.keyboard_arrow_right,
                    size: 50,
                    color: Colors.yellow,
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    ));
  }
}
